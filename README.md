# Slim&Green
* Danilo Hernández
* Omar Sanchez
* Sebastian Quintero

Para la última entrega del proyecto Slim&Green, se diseñó una primera versión de un programa funcional el cual cumpliera con dos casos de uso.

### CASOS Y SUBCASOS:
	
Tomando en cuenta las recomendaciones realizadas por el profesor después de la primera entrega, se definieron los casos y subcasos de la siguiente manera:

#### Un cliente desea llenar el formulario acerca de gustos, preferencias y más 
* El cliente se debe loguear.
* El sistema debe verificar que el usuario exista.
* El usuario debe escoger un restaurante.
* Se puede elegir por zona geográfica.
* Se puede elegir por nombre de restaurante.
* El sistema debe verificar que el restaurante exista.
* El sistema debe verificar que el cliente sea mayor de edad.
* El sistema debe verificar que el restaurante tenga un formulario disponible.
* Si no hay formularios disponibles, el sistema debe enviar un mensaje al usuario.
* Una vez lleno el formulario, el sistema debe verificar que todas las preguntas hayan sido contestadas.
* El sistema debe verificar que el espacio de comentarios generales no esté vacío.
* El sistema debe verificar que el usuario haya asignado una calificación al lugar entre 0 y 5 estrellas.
* El sistema debe promediar la calificación dada por el usuario con las calificaciones previas.
* Si el promedio se encuentra por debajo de 3 estrellas el sistema debe enviar un mensaje de alerta al administrador del lugar (cada cierto tiempo, definido por el administrador cuando crea el formulario), si está por encima no hace nada

#### El dueño de una cafetería desea registrarla.

* El administrador debe registrarse en el sistema.
* El sistema debe validar que el usuario (admin) escogido no esté en uso.
* Si está en uso el usuario (admin), el sistema debe solicitar el uso de uno   nuevo
* El administrador debe loguearse.
* El administrador registra cafetería en el sistema.
* Debe inscribir por zona geográfica
* Debe asignar nombre
* Debe asignar teléfono
* Si ya ha inscrito algún otro punto antes debe asignar código a cada uno.
* No pueden haber dos cafeterías con el mismo nombre en la misma zona geográfica.
* El administrador debe crear uno o más formularios al momento de registrar la cafetería.
* El administrador debe establecer cada cuánto desea recibir alertas del sistema. (Ejemplo: cada 100 calificaciones, si está por debajo de 3 estrellas enviar alerta)

### Desarrollo 

Para poder cumplir con estos requerimientos creamos un programa en Java, donde se diseñaron las clases relacionadas a los usuarios (cliente, administrador, etc), a los formularios y a los puntos de comida; donde el usuario podría registrarse, inscribir un punto de comida, calificar un restaurante, registrar a otro usuario (en el caso de administrador o dueño), etc.

El programa fue enlazado con una base de datos hecha en MySQL, donde se almacenó toda la información correspondiente a los usuarios, la cual sirvió al momento de realizar las diferentes validaciones (como verificar la existencia de un usuario, o que su Nick de registro sea único).

El proyecto fue hecho en Netbeans IDE 8.1

### Para correr:
* Abrir el proyecto con NetBeans 
* Crear la base de datos apartir del codigo SQL que esta en "Dump20171119.sql"
* Finalmente, colocar las propias credenciales en src/ConexionDB/Conexion.java
