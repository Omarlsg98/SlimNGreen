/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloGUI;

import ModuloPreferencia.Enfoque_Pregunta;
import ModuloPreferencia.Pregunta;
import ModuloPreferencia.Tipo_Pregunta;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import Conectores.ConectorPreferencia;
import VistaUsuario.Administrador;

/**
 *
 * @author omarleonardo
 */
public class AñadirPreguntas extends javax.swing.JFrame {

    /**
     * Creates new form CrearFormulario
     */
    public AñadirPreguntas() {
        initComponents();
    }
    ConectorPreferencia cp;

    public void setConector(ConectorPreferencia aThis) {
        this.cp = aThis;
        for (Enfoque_Pregunta enfoque : cp.getEnfoques()) {
            Enfoques.addItem("" + enfoque.getEnfoque());
        }
        for (Tipo_Pregunta tipo : cp.getTipos()) {
            Tipos.addItem("" + tipo.getTipo());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Link = new javax.swing.JTextField();
        Pregunta = new javax.swing.JTextField();
        PosibleRespuesta = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Continuar = new javax.swing.JButton();
        NumeroPregunta = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        Enfoques = new javax.swing.JComboBox<>();
        Tipos = new javax.swing.JComboBox<>();
        AñadirPosible = new javax.swing.JButton();
        AñadirInfografia = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Añadir pregunta:");

        jLabel2.setText("Añadir link (URL) informativo (Opcional):");

        Link.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LinkActionPerformed(evt);
            }
        });

        jLabel3.setText("Escriba la pregunta:");

        jLabel4.setText("Escoja el enfoque de la pregunta:");

        jLabel5.setText("Añadir posible respuesta");

        Continuar.setText("Añadir siguiente pregunta");
        Continuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ContinuarActionPerformed(evt);
            }
        });

        NumeroPregunta.setText("1");

        jLabel7.setText("Escoja el tipo de la pregunta:");

        AñadirPosible.setText("Añadir posible respuesta");
        AñadirPosible.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AñadirPosibleActionPerformed(evt);
            }
        });

        AñadirInfografia.setText("Añadir Infografia");
        AñadirInfografia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AñadirInfografiaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(NumeroPregunta, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(283, 283, 283))
            .addGroup(layout.createSequentialGroup()
                .addGap(116, 116, 116)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Continuar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(Pregunta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(210, 210, 210))
                                    .addComponent(Link, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap(150, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel7))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Enfoques, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Tipos, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(PosibleRespuesta, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(AñadirPosible)
                        .addGap(48, 48, 48)
                        .addComponent(AñadirInfografia)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(NumeroPregunta))
                .addGap(29, 29, 29)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Link, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Pregunta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(Enfoques, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(Tipos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(PosibleRespuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AñadirPosible, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AñadirInfografia, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(102, 102, 102)
                .addComponent(Continuar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LinkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LinkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LinkActionPerformed
    Pregunta[] preguntas = new Pregunta[10];
    int counter = 0;
    private void ContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ContinuarActionPerformed
        // TODO add your handling code here:
        if (counter < 9) {
            boolean continuar = true;
            String preg = Pregunta.getText();
            String link = Link.getText();
            int enfoque = Enfoques.getSelectedIndex();
            int tipo = Tipos.getSelectedIndex();
            if (preg.isEmpty()) {
                continuar = false;
                JOptionPane.showMessageDialog(null, "No puede dejar el campo pregunta vacio");
            }
            if (continuar) {
                if (posiblesRespuestas.isEmpty()) {
                    preguntas[counter] = new Pregunta(counter, link, preg, cp.getEnfoques().get(enfoque), cp.getTipos().get(tipo));
                } else {
                    preguntas[counter] = new Pregunta(posiblesRespuestas, counter, link, preg, cp.getEnfoques().get(enfoque), cp.getTipos().get(tipo));
                }
                Pregunta.setText("");
                Link.setText("");
                Enfoques.setSelectedIndex(0);
                Tipos.setSelectedIndex(0);
                counter++;
                NumeroPregunta.setText("" + (counter + 1));
                JOptionPane.showMessageDialog(null, "Pregunta guardada");

            }
        } else {
            cp.addFormulario(desc, Aut1, Aut2, Aut3, disp, preguntas);
            JOptionPane.showMessageDialog(null, "Formulario guardado con exito");
            Administrador car = new Administrador();
            car.setVisible(true);
            this.dispose();
        }

    }//GEN-LAST:event_ContinuarActionPerformed
    ArrayList<String> posiblesRespuestas = new ArrayList<>();

    private void AñadirPosibleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AñadirPosibleActionPerformed
        // TODO add your handling code here:
        if (!PosibleRespuesta.getText().isEmpty()) {
            posiblesRespuestas.add(PosibleRespuesta.getText());
            PosibleRespuesta.setText("");
        } else {
            JOptionPane.showMessageDialog(null, "No puede añadir posibles respuestas vacias");
        }
    }//GEN-LAST:event_AñadirPosibleActionPerformed

    private void AñadirInfografiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AñadirInfografiaActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "No disponible para este demo");
    }//GEN-LAST:event_AñadirInfografiaActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AñadirInfografia;
    private javax.swing.JButton AñadirPosible;
    private javax.swing.JButton Continuar;
    private javax.swing.JComboBox<String> Enfoques;
    private javax.swing.JTextField Link;
    private javax.swing.JLabel NumeroPregunta;
    private javax.swing.JTextField PosibleRespuesta;
    private javax.swing.JTextField Pregunta;
    private javax.swing.JComboBox<String> Tipos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    // End of variables declaration//GEN-END:variables
String desc, Aut1, Aut2, Aut3;
    boolean disp;

    void setDatos(String desc, String Aut1, String Aut2, String Aut3, boolean disp) {
        this.desc = desc;
        this.Aut1 = Aut1;
        this.Aut2 = Aut2;
        this.Aut3 = Aut3;
        this.disp = disp;
    }
}
