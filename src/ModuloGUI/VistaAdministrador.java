/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloGUI;
import ModuloRestaurante.Comida;
import ModuloRestaurante.PuntoDeComida;
import ModuloRestaurante.TipoDeComida;
import ModuloRestaurante.TipoPuntoDeComida;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import Conectores.GestionPuntoDeComida;

/**
 *
 * @author Sebastian Quintero
 */
public class VistaAdministrador extends javax.swing.JFrame implements ActionListener 
{
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Slim&Green");
        setResizable(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Tipo Punto De Comida");

        jButton1.setText("Atrás");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jLabel2.setText("Punto De Comida");

        jLabel3.setText("Tipo  De Comida");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jLabel4.setText("Comida");

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable4);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(304, 304, 304)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 302, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jButton1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    // End of variables declaration//GEN-END:variables
    private DefaultTableModel tablaPuntoDeComida;
    private DefaultTableModel tablaTipoPuntoDeComida;
    private DefaultTableModel tablaComida;
    private DefaultTableModel tablaTipoDeComida;
    private  GestionPuntoDeComida gestion;
    private PrincipalRestaurante p;
    public VistaAdministrador(PrincipalRestaurante p)
    {
        this.p=p;
        initComponents();       
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        this.listen();  
        this.gestion=new GestionPuntoDeComida(this); 
        this.modelarTabla();
        this.llenarTablaPuntoDeComida();     
        this.llenarTablaTipoPuntoDeComida();
        this.llenarTablaComida();
        this.llenarTablaTipoDeComida();
    }
     
     
    

    private void listen()
    {
        this.jButton1.addActionListener(this);

        //Botones
    }

    @Override
    public void actionPerformed(ActionEvent evento) 
    {    
        if(evento.getSource()==this.jButton1)
        {
            this.setVisible(false);
             this.p.setVisible(true);
        }
       
    }
 
     private void modelarTabla()
   {
       
       this.tablaPuntoDeComida= new DefaultTableModel();
       this.jTable2.setModel(tablaPuntoDeComida);
       //AGREGAR COLUMNAS
       this.tablaPuntoDeComida.addColumn("idPuntoDeComida");
       this.tablaPuntoDeComida.addColumn("idTipoPuntoDeComida");
       this.tablaPuntoDeComida.addColumn("Nombre");
       this.tablaPuntoDeComida.addColumn("Capacidad");
       this.tablaPuntoDeComida.addColumn("Ingresos Diarios");
       this.tablaPuntoDeComida.addColumn("Descripcion");
       this.tablaPuntoDeComida.addColumn("Estrellas");
       this.tablaPuntoDeComida.addColumn("Número de Calificaciones");
       //MODIFICAR TAMAÑO COLUMNA
       TableColumn columna=null;
       columna= jTable2.getColumnModel().getColumn(0);
       columna.setMinWidth(100);
       columna.setMaxWidth(100);     
       //TABLA 2
       this.tablaTipoPuntoDeComida= new DefaultTableModel();
       this.jTable1.setModel(tablaTipoPuntoDeComida);
       //AGREGAR COLUMNAS
       this.tablaTipoPuntoDeComida.addColumn("idTipoPuntoDeComida");
       this.tablaTipoPuntoDeComida.addColumn("TipoPuntoDeComida");

       TableColumn columna1=null;
       columna1= jTable1.getColumnModel().getColumn(0);
       columna.setMinWidth(100);
       columna.setMaxWidth(100);  
      //Tabla 3
       this.tablaComida= new DefaultTableModel();
       this.jTable4.setModel(tablaComida);
       //AGREGAR COLUMNAS
       this.tablaComida.addColumn("idComida");
       this.tablaComida.addColumn("Nombre Comida");
       this.tablaComida.addColumn("Descripcion Comida");
       TableColumn columna2=null;
       columna2= jTable4.getColumnModel().getColumn(0);
       columna.setMinWidth(100);
       columna.setMaxWidth(100);  
       //Tabla 4
        this.tablaTipoDeComida= new DefaultTableModel();
       this.jTable3.setModel(tablaTipoDeComida);
       //AGREGAR COLUMNAS
       this.tablaTipoDeComida.addColumn("idTipoDeComida");
       this.tablaTipoDeComida.addColumn("Tipo de Comida");
       TableColumn columna3=null;
       columna3= jTable4.getColumnModel().getColumn(0);
       columna.setMinWidth(100);
       columna.setMaxWidth(100);  
   }
     private void llenarTablaPuntoDeComida()
    {
      
       ArrayList<PuntoDeComida> pc=this.gestion.extraerPuntoDeComida();
           
       for(PuntoDeComida ab: pc)
       {
            Vector fila= new Vector();
            fila.add(ab.getIdPuntoDeComida());
            fila.add(ab.getIdTipoPuntoDeComida());
            fila.add(ab.getNombre());
            fila.add(ab.getCapacidad());
            fila.add(ab.getIngresosDiarios());
            fila.add(ab.getDescripcion());
            fila.add(ab.getEstrellas());
            fila.add(ab.getNumeroCalificaciones());
            this.tablaPuntoDeComida.addRow(fila);

        }
        
    }
    private void llenarTablaTipoPuntoDeComida()
    {
      
       ArrayList<TipoPuntoDeComida> tpc=this.gestion.extraerTipoPuntoDeComida();
           
       for(TipoPuntoDeComida ab: tpc)
       {
            Vector fila= new Vector();
            fila.add(ab.getIdTipoPuntoDeComida());
            fila.add(ab.getTipoPuntoDeComida());
            this.tablaTipoPuntoDeComida.addRow(fila);

        }
        
    }
     private void llenarTablaComida()
    {
      
       ArrayList<Comida> tpc=this.gestion.extraerComida();
           
       for(Comida ab: tpc)
       {
            Vector fila= new Vector();
            fila.add(ab.getIdComida());
            fila.add(ab.getNombre());
            fila.add(ab.getDescripcion());
            this.tablaComida.addRow(fila);

        }
        
    }
         private void llenarTablaTipoDeComida()
    {
      
       ArrayList<TipoDeComida> tpc=this.gestion.extraerTipodDeComida();
           
       for(TipoDeComida ab: tpc)
       {
            Vector fila= new Vector();
            fila.add(ab.getIdTipoDeComida());
            fila.add(ab.getTipoDeComida());

            this.tablaTipoDeComida.addRow(fila);

        }
        
    }
    //Métodos de servicio
     
   
}
