/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloPreferencia;

import java.util.Date;
import ModuloUsuario.Persona;

/**
 *
 * @author omarleonardo
 */
public class Formulario {

    private int idFormulario;
    private Date fecha;
    private boolean disponible;
    private String descripcion;
    private Pregunta[] preguntas = new Pregunta[10];
    private Persona[] autores = new Persona[3];

    public Formulario(int idFormulario, Date fecha, boolean disponible, String descripcion, Pregunta[] preguntas) {
        if (preguntas.length==10){
            this.idFormulario = idFormulario;
            this.fecha = fecha;
            this.disponible = disponible;
            this.descripcion = descripcion;
            this.preguntas= preguntas;
        }else{
            System.out.println("Error, deben ser 10 preguntas");
        }
    }

    public void cambiarDisponibilidad(boolean disponible) {
        this.disponible = disponible;
    }

    public void añadirCambiarPregunta(Pregunta pregunta, int posicion) {
        this.preguntas[posicion] = pregunta;
    }

    public int getIdFormulario() {
        return idFormulario;
    }

    public void setIdFormulario(int idFormulario) {
        this.idFormulario = idFormulario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Pregunta[] getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(Pregunta[] preguntas) {
        this.preguntas = preguntas;
    }

    public Persona[] getAutores() {
        return autores;
    }

    public void setAutores(Persona[] autores) {
        this.autores = autores;
    }

}
