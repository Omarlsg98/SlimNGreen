/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloPreferencia;

/**
 *
 * @author omarleonardo
 */
public class Respuesta {
    private int id_Respuesta;
    private String respuesta;

    public Respuesta(int id_Respuesta, String respuesta) {
        this.id_Respuesta = id_Respuesta;
        this.respuesta = respuesta;
    }

    public int getId_Respuesta() {
        return id_Respuesta;
    }

    public void setId_Respuesta(int id_Respuesta) {
        this.id_Respuesta = id_Respuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    
    
}
