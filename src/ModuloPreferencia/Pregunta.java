/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloPreferencia;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author omarleonardo
 */
public class Pregunta {

    private int id_Pregunta;
    private String link;
    private String enunciado;
    private ArrayList<String> posiblesRespuestas = new ArrayList<>();
    private ArrayList<Image> infografia = new ArrayList<>();
    private Enfoque_Pregunta enfoque;
    private Tipo_Pregunta tipo;
    private ArrayList<Respuesta> respuestas = new ArrayList<>();

    public Pregunta(int id_Pregunta, String link, String enunciado, Enfoque_Pregunta enfoque, Tipo_Pregunta tipo) {
        this.id_Pregunta = id_Pregunta;
        this.link = link;
        this.enunciado = enunciado;
        this.enfoque = enfoque;
        this.tipo = tipo;
    }

    public Pregunta(ArrayList<String> posiblesRespuestas, int id_Pregunta, String link, String enunciado, Enfoque_Pregunta enfoque, Tipo_Pregunta tipo) {
        if (!posiblesRespuestas.isEmpty()) {
            this.id_Pregunta = id_Pregunta;
            this.link = link;
            this.enunciado = enunciado;
            this.enfoque = enfoque;
            this.tipo = tipo;
            this.posiblesRespuestas = posiblesRespuestas;
        } else {
            System.out.println("Error: posibles respuestas esta vacio");
        }
    }

    public void añadirPosibleRespuesta(String posibleRespuesta) {
        this.posiblesRespuestas.add(posibleRespuesta);
    }

    public void añadirRespuesta(Respuesta respuesta) {
        this.respuestas.add(respuesta);
    }

    public int getId_Pregunta() {
        return id_Pregunta;
    }

    public void setId_Pregunta(int id_Pregunta) {
        this.id_Pregunta = id_Pregunta;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public ArrayList<String> getPosiblesRespuestas() {
        return posiblesRespuestas;
    }

    public void setPosiblesRespuestas(ArrayList<String> posiblesRespuestas) {
        this.posiblesRespuestas = posiblesRespuestas;
    }

    public ArrayList<Image> getInfografia() {
        return infografia;
    }

    public void setInfografia(ArrayList<Image> infografia) {
        this.infografia = infografia;
    }

    public Enfoque_Pregunta getEnfoque() {
        return enfoque;
    }

    public void setEnfoque(Enfoque_Pregunta enfoque) {
        this.enfoque = enfoque;
    }

    public Tipo_Pregunta getTipo() {
        return tipo;
    }

    public void setTipo(Tipo_Pregunta tipo) {
        this.tipo = tipo;
    }

    public ArrayList<Respuesta> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(ArrayList<Respuesta> respuestas) {
        this.respuestas = respuestas;
    }

}
