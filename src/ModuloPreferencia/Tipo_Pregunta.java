/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloPreferencia;

/**
 *
 * @author omarleonardo
 */
public class Tipo_Pregunta {
    private int id_pregunta;
    private String tipo;

    public Tipo_Pregunta(int id_pregunta, String tipo) {
        this.id_pregunta = id_pregunta;
        this.tipo = tipo;
    }                                   

    public int getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
