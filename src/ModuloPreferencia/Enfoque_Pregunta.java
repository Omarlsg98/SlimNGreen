/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloPreferencia;

/**
 *
 * @author omarleonardo
 */
public class Enfoque_Pregunta {
    private int id_enfoque;
    private String enfoque;

    public Enfoque_Pregunta(int id_enfoque, String enfoque) {
        this.id_enfoque = id_enfoque;
        this.enfoque = enfoque;
    }

    public int getId_enfoque() {
        return id_enfoque;
    }

    public void setId_enfoque(int id_enfoque) {
        this.id_enfoque = id_enfoque;
    }

    public String getEnfoque() {
        return enfoque;
    }

    public void setEnfoque(String enfoque) {
        this.enfoque = enfoque;
    }
    
    
    
}
