/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cpe
 */
public class SqlUsuarios extends Conexion {
    
    public boolean registrar(usuarios usr)
    {
        PreparedStatement ps = null;
        Connection con= getConexion();
        
        String sql="Insert INTO usuarios(usuario,password,correo,cedula,telefono,id_tipo) VALUES(?,?,?,?,?,?)";
        
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1, usr.getUsuario());
            ps.setString(2, usr.getPassword());
            ps.setString(3, usr.getCorreo());
            ps.setString(4, usr.getCedula());
            ps.setString(5, usr.getTelefono());
            ps.setInt(5, usr.getId_tipo());
            ps.execute();
            return true;
            
        } catch (SQLException ex) {
            Logger.getLogger(SqlUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
}
