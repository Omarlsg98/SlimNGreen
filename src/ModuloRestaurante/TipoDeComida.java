/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloRestaurante;

/**
 *
 * @author Sebastian Quintero
 */
public class TipoDeComida
{
    public String idTipoDeComida;
    private String tipoDeComida;
    
    public TipoDeComida()
    {

        this.idTipoDeComida="";
        this.tipoDeComida="";
 
        
    }
    public TipoDeComida(String idTipoDeComida,String tipoDeComida)
    { 
        this.idTipoDeComida=idTipoDeComida;
        this.tipoDeComida=tipoDeComida;
    }

    public String getIdTipoDeComida() {
        return idTipoDeComida;
    }

    public String getTipoDeComida() {
        return tipoDeComida;
    }

    public void setIdTipoDeComida(String idTipoDeComida) {
        this.idTipoDeComida = idTipoDeComida;
    }

    public void setTipoDeComida(String tipoDeComida) {
        this.tipoDeComida = tipoDeComida;
    }

   
   
   

 @Override
   public String toString()
    {
        return this.idTipoDeComida+","+this.tipoDeComida;
    }
   

   
   
}
