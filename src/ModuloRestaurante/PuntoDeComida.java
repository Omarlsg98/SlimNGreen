/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloRestaurante;

/**
 *
 * @author Sebastian Quintero
 */
public class PuntoDeComida
{
    private String idPuntoDeComida;
    public String idTipoPuntoDeComida;
    private String nombre;
    private String capacidad;
    private String ingresosDiarios;
    private String foto;
    private String descripcion;
    private String estrellas;
    private String numeroCalificaciones;

    public PuntoDeComida()
    {
        this.idPuntoDeComida="";
        this.idTipoPuntoDeComida="";
        this.nombre="";
        this.capacidad="";
        this.ingresosDiarios="";
        this.foto="";
        this.descripcion="";
        this.estrellas="";
        this.numeroCalificaciones="";
        
    }
    public PuntoDeComida(String idPuntoDeComida,String tipoDeComida,String nombre, String capacidad, String ingresosDiarios, String foto, String descripcion, String estrellas, String numeroCalificaciones)
    {
        this.idPuntoDeComida=idPuntoDeComida;
        this.idTipoPuntoDeComida=tipoDeComida;
        this.nombre=nombre;
        this.capacidad=capacidad;
        this.ingresosDiarios=ingresosDiarios;
        this.foto=foto;
        this.descripcion=descripcion;
        this.estrellas=estrellas;
        this.numeroCalificaciones=numeroCalificaciones;
    }

    public String getIdTipoPuntoDeComida() {
        return idTipoPuntoDeComida;
    }

    public String getTipoDeComida() {
        return idTipoPuntoDeComida;
    }

    public String getIdPuntoDeComida() {
        return idPuntoDeComida;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public String getIngresosDiarios() {
        return ingresosDiarios;
    }

    public String getFoto() {
        return foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getEstrellas() {
        return estrellas;
    }

    public String getNumeroCalificaciones() {
        return numeroCalificaciones;
    }

    public void setIdPuntoDeComida(String idPuntoDeComida) {
        this.idPuntoDeComida = idPuntoDeComida;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public void setIngresosDiarios(String ingresosDiarios) {
        this.ingresosDiarios = ingresosDiarios;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setEstrellas(String estrellas) {
        this.estrellas = estrellas;
    }

    public void setNumeroCalificaciones(String numeroCalificaciones) {
        this.numeroCalificaciones = numeroCalificaciones;
    }

    public void setTipoDeComida(String tipoDeComida) {
        this.idTipoPuntoDeComida = tipoDeComida;
    }

    public void setIdTipoPuntoDeComida(String idTipoPuntoDeComida) {
        this.idTipoPuntoDeComida = idTipoPuntoDeComida;
    }

   

 @Override
   public String toString()
    {
        return this.idPuntoDeComida+","+this.idTipoPuntoDeComida+","+this.nombre+","+this.capacidad+","+this.ingresosDiarios+","+this.foto+","+this.descripcion+","+this.estrellas+","+this.numeroCalificaciones;
    }

 
   

   
   
}
