/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloRestaurante;

/**
 *
 * @author Sebastian Quintero
 */
public class Comida
{
    private String idComida;
    private String nombre;
    private String descripcion;
    private String foto;
    
    public Comida()
    {
        this.idComida="";
        this.nombre="";
        this.descripcion="";
        this.foto="";        
    }
    public Comida(String idComida,String nombre,String descripcion, String foto)
    { 
        this.idComida=idComida;
        this.nombre=nombre;
        this.descripcion=descripcion;
        this.foto=foto;    
    }

    public String getIdComida() {
        return idComida;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setIdComida(String idComida) {
        this.idComida = idComida;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    

 @Override
   public String toString()
    {
        return this.idComida+","+this.nombre+","+this.descripcion+","+this.foto;
    }
   

   
   
}
