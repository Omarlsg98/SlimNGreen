/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloRestaurante;

/**
 *
 * @author Sebastian Quintero
 */
public class TipoPuntoDeComida
{
    public String idTipoPuntoDeComida;
    private String tipoPuntoDeComida;
    
    public TipoPuntoDeComida()
    {

        this.idTipoPuntoDeComida="";
        this.tipoPuntoDeComida="";
 
        
    }
    public TipoPuntoDeComida(String idTipoPuntoDeComida,String tipoPuntoDeComida)
    { 
        this.idTipoPuntoDeComida=idTipoPuntoDeComida;
        this.tipoPuntoDeComida=tipoPuntoDeComida;
    }

    public String getIdTipoPuntoDeComida() {
        return idTipoPuntoDeComida;
    }

    public String getTipoPuntoDeComida() {
        return tipoPuntoDeComida;
    }

    public void setIdTipoPuntoDeComida(String idTipoPuntoDeComida) {
        this.idTipoPuntoDeComida = idTipoPuntoDeComida;
    }

    public void setTipoPuntoDeComida(String tipoPuntoDeComida) {
        this.tipoPuntoDeComida = tipoPuntoDeComida;
    }

   
   

 @Override
   public String toString()
    {
        return this.idTipoPuntoDeComida+","+this.tipoPuntoDeComida;
    }
   

   
   
}
