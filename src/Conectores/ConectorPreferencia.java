/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conectores;

import ModuloGUI.CrearFormulario;
import ModuloGUI.ResponderPreferencia;
import ModuloPreferencia.Enfoque_Pregunta;
import ModuloPreferencia.Formulario;
import ModuloPreferencia.Pregunta;
import ModuloPreferencia.Respuesta;
import ModuloPreferencia.Tipo_Pregunta;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author omarleonardo
 */
public class ConectorPreferencia {
    
    private String usuario;
    private int tipoUsuario;
    private ArrayList<Formulario> formularios = new ArrayList<>();
    private ArrayList<Tipo_Pregunta> tipos = new ArrayList<>();
    private ArrayList<Enfoque_Pregunta> enfoques = new ArrayList<>();
    
    public ConectorPreferencia(String usuario, int tipoUsuario) {
        this.usuario = usuario;
        this.tipoUsuario = tipoUsuario;
        cargarDatos();
    }
        
    public String getUsuario() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public int getTipoUsuario() {
        return tipoUsuario;
    }
    
    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    
    public ArrayList<Formulario> getFormularios() {
        return formularios;
    }
    
    public void setFormularios(ArrayList<Formulario> formualarios) {
        this.formularios = formualarios;
    }
    
    public ArrayList<Tipo_Pregunta> getTipos() {
        return tipos;
    }
    
    public void setTipos(ArrayList<Tipo_Pregunta> tipos) {
        this.tipos = tipos;
    }
    
    public ArrayList<Enfoque_Pregunta> getEnfoques() {
        return enfoques;
    }
    
    public void setEnfoques(ArrayList<Enfoque_Pregunta> enfoques) {
        this.enfoques = enfoques;
    }
    
    public void addFormulario(String desc, String Aut1, String Aut2, String Aut3, boolean disp, Pregunta[] preguntas) {
        
        formularios.add(new Formulario(formularios.size(), null, disp, desc, preguntas));
    }
    
    public void CrearFormulario(ConectorPreferencia cp) {
        CrearFormulario cf = new CrearFormulario();
        cf.setVisible(true);
        cf.setConector(cp);
    }
    
    public void LlenarFormulario(ConectorPreferencia cp) {
        
        Formulario form = formularios.get(formularios.size()-1);
        if (form.isDisponible()) {
            ResponderPreferencia rp = new ResponderPreferencia();
            rp.setup(cp, form);
            rp.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Error: Formulario no disponible");
        }
        
    }
    
    private void cargarDatos() {
        tipos.add(new Tipo_Pregunta(0, "Abierta"));
        tipos.add(new Tipo_Pregunta(1, "Seleccion Multiple unica respuesta"));
        tipos.add(new Tipo_Pregunta(2, "Seleccion Multiple respuesta multiple"));
        tipos.add(new Tipo_Pregunta(3, "Si/No"));
        enfoques.add(new Enfoque_Pregunta(0, "Salud"));
        enfoques.add(new Enfoque_Pregunta(1, "Ambiente"));
        enfoques.add(new Enfoque_Pregunta(2, "Comida"));
        enfoques.add(new Enfoque_Pregunta(3, "Servicio"));
        Pregunta[] preguntas = new Pregunta[10];
        String[] enunciados = {"¿Cual es su comida favorita?",
            "¿A que termino prefiere su carne?",
            "¿La gastronomia de que pais prefiere?",
            "¿Que acompañamiento le gusta mas?",
            "Para un menu de comidas rapidas ¿Que le gustaria que estuviera incluido?",
            "¿Que jugos son los que mas le gustan?",
            "¿Que postres le gustan mas?",
            "¿Que sopas le gustan mas?",
            "¿Como prefiere sus ensaladas?",
            "¿Que otras bebidas le gustaria que estuviera en el menu?"};
        for (int i = 0; i < 10; i++) {
            preguntas[i] = new Pregunta(i, null, enunciados[i], enfoques.get(3), tipos.get(0));
        }
        addFormulario("La presente encuesta tiene como objetivo conocer los gustos mas generales de nuestros usuarios en materia de alimentacion, hace un breve repaso de todos los tipos de comida y formas como le gusta que este preparada la comida",
                "Yo", "Yo", "Yo", true, preguntas);
    }
    
    public void AgregarRespuestas(Respuesta[] answers, int idFormulario) {
        Formulario form = formularios.get(idFormulario);
        int idRespuesta = form.getPreguntas()[0].getRespuestas().size();
        for (int i = 0; i < 10; i++) {
            answers[i].setId_Respuesta(idRespuesta);
            form.getPreguntas()[i].añadirRespuesta(answers[i]);
        }
    }

    
}
