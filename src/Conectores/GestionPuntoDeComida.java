/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conectores;


import ModuloGUI.VistaAdministrador;
import ModuloRestaurante.Comida;
import ModuloRestaurante.PuntoDeComida;
import ModuloRestaurante.TipoDeComida;
import ModuloRestaurante.TipoPuntoDeComida;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author Sebastian Quintero
 */
public class GestionPuntoDeComida 
{
    private String archivoPuntoDeComida;
    private String archivoTipoPuntoDeComida;
    private String archivoComida;
    private String archivoTipoDeComida;
    private VistaAdministrador p;
    ArrayList<PuntoDeComida> puntoDeComida;
      
    
    public GestionPuntoDeComida(VistaAdministrador p)
    {
       this.puntoDeComida= new ArrayList();
       this.p=p;
       this.archivoPuntoDeComida="./archivos/archivoPuntoDeComida.txt";
       this.archivoTipoPuntoDeComida="./archivos/archivoTipoPuntoDeComida.txt";
       this.archivoComida="./archivos/archivoComida.txt";
       this.archivoTipoDeComida="./archivos/archivoTipoDeComida.txt";
       this.verificaArchivo();
 
    }

       
    public GestionPuntoDeComida()
    {
       this.puntoDeComida= new ArrayList();
       this.archivoPuntoDeComida="./archivos/archivoPuntoDeComida.txt";
       this.archivoTipoPuntoDeComida="./archivos/archivoTipoPuntoDeComida.txt";
       this.archivoComida="./archivos/archivoComida.txt";
       this.archivoTipoDeComida="./archivos/archivoTipoDeComida.txt";
       this.verificaArchivo();
 
    }
   public void recargarArchivo(ArrayList<PuntoDeComida>students)
    {
        File file=new File(archivoPuntoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,false); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            for(PuntoDeComida stud: students)
            ps.println(stud);
            ps.close();
       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace();
       }
    }
     public void recargarArchioPuntoDeCOmida(ArrayList<PuntoDeComida>puntoDeComida)
    {
        File file=new File(archivoPuntoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,false); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            for(PuntoDeComida pc: puntoDeComida)
            ps.println(pc);
            ps.close();
       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace();
       }
    }
     public void recargarArchioTipoPuntoDeComida(ArrayList<TipoPuntoDeComida>tipoPuntoDeComida)
    {
        File file=new File(archivoTipoPuntoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,false); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            for(TipoPuntoDeComida tpc: tipoPuntoDeComida)
            ps.println(tpc);
            ps.close();
       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace();
       }
    }
        public void recargarArchioComida(ArrayList<Comida>comida)
    {
        File file=new File(archivoComida);
       try
       {
            FileWriter fr=new FileWriter(file,false); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            for(Comida c: comida)
            ps.println(c);
            ps.close();
       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace();
       }
    }
            public void recargarArchioTipoDeComida(ArrayList<TipoDeComida>tipoDeComida)
    {
        File file=new File(archivoTipoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,false); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            for(TipoDeComida tc: tipoDeComida)
            ps.println(tc);
            ps.close();
       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace();
       }
    }
        

    public void verificaArchivo()
    {//Transaccion Proteccion con ---> Try catch
        try
        {
            File objetoFile= new File(this.archivoPuntoDeComida);
            if(!objetoFile.exists())
            {
                objetoFile.createNewFile();
            }
        }
        catch(/*Tipos de excepciones en Java*/IOException xxx)
        {
            JOptionPane.showMessageDialog(null,"Hay un inconveniento, estamos trabajando en ello ...");
            xxx.printStackTrace();
        }
        try
        {
            File objetoFile= new File(this.archivoTipoPuntoDeComida);
            if(!objetoFile.exists())
            {
                objetoFile.createNewFile();
            }
        }
        catch(/*Tipos de excepciones en Java*/IOException xxx)
        {
            JOptionPane.showMessageDialog(null,"Hay un inconveniento, estamos trabajando en ello ...");
            xxx.printStackTrace();
        }
        try
        {
            File objetoFile= new File(this.archivoComida);
            if(!objetoFile.exists())
            {
                objetoFile.createNewFile();
            }
        }
        catch(/*Tipos de excepciones en Java*/IOException xxx)
        {
            JOptionPane.showMessageDialog(null,"Hay un inconveniento, estamos trabajando en ello ...");
            xxx.printStackTrace();
        }
        try
        {
            File objetoFile= new File(this.archivoTipoDeComida);
            if(!objetoFile.exists())
            {
                objetoFile.createNewFile();
            }
        }
        catch(/*Tipos de excepciones en Java*/IOException xxx)
        {
            JOptionPane.showMessageDialog(null,"Hay un inconveniento, estamos trabajando en ello ...");
            xxx.printStackTrace();
        }
    }
    public void crearPuntoDeComida(String idPuntoDeComida,String idTipoPuntoDeComida,String nombre, String capacidad, String ingresosDiarios, String foto,String descripcion,String estrellas, String numeroCalificaciones)
    {
       PuntoDeComida a=new PuntoDeComida(idPuntoDeComida,idTipoPuntoDeComida,nombre,capacidad,ingresosDiarios,foto,descripcion,estrellas,numeroCalificaciones); 
       this.guardarPuntoDeComida(a);
    }
      public void crearTipoPuntoDeComida(String idTipoPuntoDeComida,String tipoPuntoDeComida)
    {
       TipoPuntoDeComida a=new TipoPuntoDeComida(idTipoPuntoDeComida,tipoPuntoDeComida); 
       this.guardarTipoPuntoDeComida(a);
    }
        public void crearComida(String idComida,String nombre,String descripcion,String foto)
    {
       Comida a=new Comida(idComida,nombre,descripcion,foto); 
       this.guardarComida(a);
    }
    public void crearTipoDeComida(String idTipoDeComida,String TipoDeComida)
    {
       TipoDeComida tipoDeComida=new TipoDeComida(idTipoDeComida,TipoDeComida); 
       this.guardarTipoDeComida(tipoDeComida);
    }
          
    private void guardarPuntoDeComida(PuntoDeComida nuevo)
    {
          File file=new File(archivoPuntoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,true); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            ps.println(nuevo);
            ps.close();

       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace(); 
       }
    } 
    private void guardarTipoPuntoDeComida(TipoPuntoDeComida nuevo)
    {
          File file=new File(archivoTipoPuntoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,true); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            ps.println(nuevo);
            ps.close();

       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace(); 
       }
    } 
        private void guardarComida(Comida nuevo)
    {
          File file=new File(archivoComida);
       try
       {
            FileWriter fr=new FileWriter(file,true); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            ps.println(nuevo);
            ps.close();

       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace(); 
       }
    } 
       private void guardarTipoDeComida(TipoDeComida nuevo)
    {
          File file=new File(archivoTipoDeComida);
       try
       {
            FileWriter fr=new FileWriter(file,true); //False:Borrar todos e iniciar lista nueva.
            PrintWriter ps=new PrintWriter(fr);
            ps.println(nuevo);
            ps.close();

       }
       catch(IOException xxx)
       {
           JOptionPane.showMessageDialog(null,"!!!!!!!!!!!!!!Fallo en el sistema!!!!!!!!!!!!!!");
           xxx.printStackTrace(); 
       }
    } 
    public ArrayList<PuntoDeComida> extraerPuntoDeComida()
    {
        ArrayList<PuntoDeComida> todos=new ArrayList();
        FileReader file;
        BufferedReader br;
        String registro;
        try
        {
          file=new FileReader(archivoPuntoDeComida);
          br=new BufferedReader(file);
          
          while((registro=br.readLine())!=null)
          {
               String datos []= registro.split(",");                 
               PuntoDeComida puntoDeComida = new PuntoDeComida((datos[0]),datos[1],datos[2],datos[3],datos[4],datos[5],datos[6],datos[7],datos[8]);     
               todos.add(puntoDeComida);
           
          }
        }
         catch(IOException ioe)
        {
                JOptionPane.showMessageDialog(null,"error");
        }
        return todos; 
    }
   public ArrayList<TipoPuntoDeComida> extraerTipoPuntoDeComida()
    {
        ArrayList<TipoPuntoDeComida> todos=new ArrayList();
        FileReader file;
        BufferedReader br;
        String registro;
        try
        {
          file=new FileReader(archivoTipoPuntoDeComida);
          br=new BufferedReader(file);
          
          while((registro=br.readLine())!=null)
          {
               String datos []= registro.split(",");                 
               TipoPuntoDeComida tipoPuntoDeComida = new TipoPuntoDeComida((datos[0]),datos[1]);     
               todos.add(tipoPuntoDeComida);
           
          }
        }
         catch(IOException ioe)
        {
                JOptionPane.showMessageDialog(null,"error");
        }
        return todos; 
    }
   public ArrayList<Comida> extraerComida()
    {
        ArrayList<Comida> todos=new ArrayList();
        FileReader file;
        BufferedReader br;
        String registro;
        try
        {
          file=new FileReader(archivoComida);
          br=new BufferedReader(file);
          
          while((registro=br.readLine())!=null)
          {
               String datos []= registro.split(",");                 
               Comida comida = new Comida((datos[0]),datos[1],datos[2],datos[3]);     
               todos.add(comida);
           
          }
        }
         catch(IOException ioe)
        {
                JOptionPane.showMessageDialog(null,"error");
        }
        return todos; 
    }
   public ArrayList<TipoDeComida> extraerTipodDeComida()
    {
        ArrayList<TipoDeComida> todos=new ArrayList();
        FileReader file;
        BufferedReader br;
        String registro;
        try
        {
          file=new FileReader(archivoTipoDeComida);
          br=new BufferedReader(file);
          
          while((registro=br.readLine())!=null)
          {
               String datos []= registro.split(",");                 
               TipoDeComida tipoDeComida = new TipoDeComida((datos[0]),datos[1]);     
               todos.add(tipoDeComida);
           
          }
        }
         catch(IOException ioe)
        {
                JOptionPane.showMessageDialog(null,"error");
        }
        return todos; 
    }
}
