/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModuloExperiencia;

import ModuloRestaurante.PuntoDeComida;
import ModuloUsuario.Cliente;

/**
 *
 * @author omarleonardo
 */
public class Evaluacion_Servicio {
    private int estrellas;
    private String comentario;
    private Cliente evaluador;
    private PuntoDeComida pcEvaluado;

    public Evaluacion_Servicio(int estrellas, String comentario, Cliente evaluador, PuntoDeComida pcEvaluado) {
        this.estrellas = estrellas;
        this.comentario = comentario;
        this.evaluador = evaluador;
        this.pcEvaluado = pcEvaluado;
    }

    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Cliente getEvaluador() {
        return evaluador;
    }

    public void setEvaluador(Cliente evaluador) {
        this.evaluador = evaluador;
    }

    public PuntoDeComida getPcEvaluado() {
        return pcEvaluado;
    }

    public void setPcEvaluado(PuntoDeComida pcEvaluado) {
        this.pcEvaluado = pcEvaluado;
    }
   
    
    
}
